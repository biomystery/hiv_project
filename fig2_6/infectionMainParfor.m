%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% infectionMain: perturbation selected parameters by modifier 
%% Time-stamp: <2013-08-04 Sun 06:12:54 UTC>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('~/Dropbox/matlab');
%% Original  parameters, unit min
load par.mat; 
nVirion = 5000;


modifier = [1/8 1/4 1/2 1 2 4 8]; 


%% 1. deg rate constant 

pnorm = p(9); 
prun = repmat(p,numel(modifier)); 
prun(:,9) = modifier'*pnorm;

 parfor i =1: numel(modifier) 
     [rateDeg(i) tmp]= infectionSingle(prun(i,:),nVirion); 
     rateFDtime{i} =tmp; 
 end 
% 
%% 2. traffic
pnorm = p(8); 
prun = repmat(p,numel(modifier)); 
prun(:,8) = modifier'*pnorm;

parfor i =1: numel(modifier) 
    [rateTra(i) tmp]= infectionSingle(prun(i,:),nVirion); 
    rateTraFDtime{i} =tmp; 
end


%% 3. fusion

pnorm = p(5); 
prun = repmat(p,numel(modifier)); 
prun(:,5) = modifier'*pnorm;

parfor i =1: numel(modifier) 
    [rateFusion(i) tmp] = infectionSingle(prun(i,:),nVirion); 
    rateFusionFDtime{i}=tmp; 
end

%% 4. RT
pnorm = p(10); 
prun = repmat(p,numel(modifier)); 
prun(:,10) = modifier'*pnorm;

parfor i =1: numel(modifier) 
    [rateRT(i) tmp] = infectionSingle(prun(i,:),nVirion); 
    rateRTFDtime{i} = tmp; 
end

%% 5. Integration
pnorm = p(14); 
prun = repmat(p,numel(modifier)); 
prun(:,14) = modifier'*pnorm;

parfor i =1: numel(modifier) 
    [rateIntegration(i) tmp] = infectionSingle(prun(i,:),nVirion); 
    rateInteFDtime{i} = tmp; 
end

save 20151231_0900am_rate50000.mat



