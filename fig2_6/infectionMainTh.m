%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% infectionMainTh: perturbation selected parameters by modifier and
%%                  assess the fate-decision by applying a time threshold
%% Time-stamp: <2014-01-17 Fri 14:37:26 PST>
%% Dependency: 
%%      1.function inteFracNorm = infectionSingle(p,nVirion)
%%      2. function inteFracTh(id) =
%%         inteSensTh(p,id,perturbFrac,nVirion,inteFracNorm); 
%%      3. pars.mat 
%%      4. parName.mat 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Original  parameters, unit min
clc; clear; close all;
tic
load par.mat
nVirion = 5000;
inteFracNorm = infectionSingle(p,nVirion)

perturbFrac = 0.02;

for id = 1:20
    disp(id)
    inteFracTh(id) = inteSensTh(p,id,perturbFrac,nVirion,inteFracNorm);
end
toc

%%
load parName.mat
inteFracThAbs = abs(inteFracTh);
[B IX] = sort(inteFracThAbs);

%% visualization 
figure('position',[1         1        1039         879]);
b1 = barh(B); 
colr = colormap(summer); 
set(gca,'ylim',[0 21],'ytick',1:20)

for i = 1:20
    name{i}=parName{IX(i)};
end 
set(gca,'yticklabel',name);


title('Sensitivity of integrate faction at 24h PI')

xlabel('Sensitivity')