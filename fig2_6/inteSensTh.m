
function inteFracSens = inteSensTh(p,id,perturbFrac,nVirion,inteFracNorm)

%id = 9 ;
perturbFrac = 0.05;
p(id) = p(id)* (1 + perturbFrac);
inteFrac = infectionSingle(p,nVirion);
inteFracSens = (inteFrac - inteFracNorm)/perturbFrac;

