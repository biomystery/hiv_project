load r200.mat
figure('position',[30 37 1035 903])

%
for i = 1:nVirion
    % count death rate and survival rate
    % rw time course
    subplot(3,2,[3 4])

    colr(i,:)=[rand rand rand];


    plot(rw{i}(:,1),rw{i}(:,2),'color',colr(i,:));
    title('Random walk trajectories of individual virion on MT','color','r')
    hold on ;

    xlabel('Time (s)')
    ylabel('x (\mum)')

    axis tight

    % mtarrive
    subplot(3,2,1)
    plot(MTarrive(i),i,'.','color',colr(i,:),'markersize',20)
    hold on
    xlabel('MT arrive time (min)')
    ylabel('virion id')

end
% identify the survival virus
subplot(3,2,[3 4])

hold on;
for i = 1:(count-1)
    plot(rw{arriveId(i)}(end,1),rw{arriveId(i)}(end,2),'s',...
        'MarkerEdgeColor','none',...
                'MarkerFaceColor',colr(arriveId(i),:),...
                'MarkerSize',10)%

end


%% MT stay time
h2=subplot(3,2,5)
cla(h2)
%
for i = 1:(countd-1)
    plot(MTstay(deathId(i)),deathId(i),'.',...
        'color',colr(deathId(i),:),'markersize',20)
    hold on
end
%
for i = 1:(count-1)
    plot(MTstay(arriveId(i)),arriveId(i),'s',...
        'MarkerEdgeColor','none',...
                'MarkerFaceColor',colr(arriveId(i),:),...
                'MarkerSize',5)%

end
 xlabel('MT stay time (min)')
 ylabel('Virion id')
%% hist of mt arrive
subplot(3,2,2)
hist(MTarrive(1:nVirion),16)
xlabel('MT arrive time (min)')
ylabel('Counts')
set(gca,'box','off','TickDir','out')
% hist of mt stay
subplot(3,2,6)
hist(MTstay(1:nVirion),16)
xlabel('MT stay time (min)')
ylabel('Counts')
set(gca,'box','off','TickDir','out')

%% get data from fig3 (A): histogram 
clear all; close all; 
hgload('200virion1.fig')
subf = get(gcf,'children');
dataf = get(subf(2),'Children');
xd = get(dataf,'XData');
bincenters = mean(xd(2:3,:)); 
xd = cell2mat(get(get(subf(3),'children'),'xdata'));
xd = cell2mat(get(get(subf(4),'children'),'xdata'));

% gamma distribution fit 
axes(subf(2))
addpath('~/Dropbox/matlab');
[H,pd] = histfitm(xd,numel(bincenters),'gamma');

xlabel('MT arrive time (min)') 
ylabel('Counts')
%
text(200,50,'Gamma distribution:','color','r')
text(220,42,['Shape = ',num2str(round(pd.a*100)/100)],'color','r')
text(220,42-5,['Scale = ',num2str(round(pd.b*100)/100)],'color','r')

%%
figDir = '~/Dropbox/Current/3.Hiv_project/HIVpaper/2nd_submission/figures/';
save2pdf([figDir, 'fig2.pdf'])



%% fig 3
nVirion = size(xd,1)

figure('position',[30 37 1035 903])
%%
for i = 1 : nVirion
    % Fate
    subplot(2,2,3)
    plot(Fate(i)+(rand-0.5)*0.5,i,'.','color',colr(i,:),'markersize',20)
    hold on
    %     hist(Fate)
    axis tight
    set(gca,'xtick',1:4,'xticklabel',{'2LTR','1LTR','Integrated','Degraded'})
    ylabel('Virion id')
end

% label the bar
[a b] = hist(Fate,4);
gtext(num2str(a(1)),'color','r')
gtext(num2str(a(2)),'color','r')
gtext(num2str(a(3)),'color','r')
gtext(num2str(a(4)),'color','r')

%% fig.2b
subplot(2,2,1)
hist(FateDtime(deathId(:)),0:10:400)
xlabel('Total time (min)')
ylabel('Counts')
axis tight
 set(gca,'box','off','TickDir','out')

%%
% disp('Survival rate = ')
% disp(1-a(4)/200)
%% fig.2d
subplot(2,2,4)

h = pie(a)
set(h(1),'FaceColor','b')
set(h(3),'FaceColor','g')
set(h(5),'FaceColor','r')
set(h(7),'FaceColor','y')

names = {'Degraded','Integrated','1LTR','2LTR'};
for i =1 :4
    gtext(names{i},'color','w')
end
title('Final fate','color','r')

%% fig.2b
subplot(2,2,2)
%hist(FateDtime,0:60:1920)
hist(FateDtime(arriveId(:)),0:120:1920)

ylabel('Counts')
axis tight
set(gca,'box','off','TickDir','out','xtick',0:240:1920,'xticklabel',0:4:32)

%%
% = findobj(gca,'Type','patch');
%set(h,'FaceColor','k','EdgeColor','w','facealpha',1)
%%
hold on
%%
% hist(FateDtime(arriveId(:)),0:60:3000)
% h = findobj(gca,'Type','patch');
% set(h,'FaceColor','y','EdgeColor','k','facealpha',0.75);



% %%
% open 200virions2.fig
%
% %%
% saveas(gca,'modify.fig')
% saveEps('modify.eps')
%
% %%
% close all
