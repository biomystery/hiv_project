
function ytmp = runSet2(y0,p)

%% parameters, unit min

% set 2
kdeg6 = p(9)     ;%deg4 = 0.002; % degradation 
krt7  = p(10)    ;% krt7 = p(9); % RT rate
% other
V = p(17);      % cell size

%% init
ytmp(1,1)  = y0(1);  % vironfree
ytmp(1,2)  = y0(2);  % cd4pm
ytmp(1,3)  = y0(3);  % ccr5pm
ytmp(1,4)  = y0(4);  % viron:cd4
ytmp(1,5)  = y0(5);  % viron:cd4:ccr
ytmp(1,6)  = y0(6);  % virion(x)
ytmp(1,7)  = y0(7);  % RTC(x)
ytmp(1,8)  = y0(8);  % PIC
ytmp(1,9)  = y0(9);  % npcnuc
ytmp(1,10) = y0(10); % picdoc
ytmp(1,11) = y0(11); % PICnuc
ytmp(1,12) = y0(12); % vironint
ytmp(1,13) = y0(13); % 1LTRnuc
ytmp(1,14) = y0(14); % 2LTRnuc


i = 1;
t(i) = 0 ; 

%% Run reactions set 2
p6 =     kdeg6 * ytmp(i,7);  % deg
p7 = p6 + krt7 * ytmp(i,7);  % rt

% update state
s2 = rand * p7;

z6 =  (s2<=p6);% deg
z7 =  (s2> p6);% rt

% update time
i    = i + 1;
t(i) = t(i-1) - log(rand)/p7;

ytmp(i,1)  = ytmp(i-1,1);                       % vironfree
ytmp(i,2)  = ytmp(i-1,2);                       % cd4
ytmp(i,3)  = ytmp(i-1,3);                       % ccr5
ytmp(i,4)  = ytmp(i-1,4);                       % vrion:cd4
ytmp(i,5)  = ytmp(i-1,5);                       % vrion:cd4:ccr5
ytmp(i,6)  = ytmp(i-1,6);                       % virionx=0
ytmp(i,7)  = max(0,ytmp(i-1,7)  - z6 - z7 );    % RTC(x)
ytmp(i,8)  = max(0,ytmp(i-1,8)  + z7 );         % PIC
ytmp(i,9)  = ytmp(i-1,9);
ytmp(i,10) = ytmp(i-1,10);
ytmp(i,11) = ytmp(i-1,11);
ytmp(i,12) = ytmp(i-1,12);
ytmp(i,13) = ytmp(i-1,13);
ytmp(i,14) = ytmp(i-1,14);

%% output
ytmp  = [t' ytmp]; % 1th column is the time

end

