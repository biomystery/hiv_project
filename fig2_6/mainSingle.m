% mainSingle:
% Run the model many times and get the statistics. Each time uses one virus
% infect one CD4+ T cell. 

%% run the simulation
clear;clc



%% parameters, unit min
% % % parameters, unit min
% p(1)  = 6.72e-18; %kb1 = p(1); % binding to CD4
% p(2)  = 0.09;    %kd1 = p(2);     % unbinding from cd4
% 
% p(3)  = 9e-18;   %kb2 = p(3);    % binds to the ccr5
% 
% p(4)  = log(2)/32;%kd2 = p(4);% unbind from the ccr5
% 
% p(5)  = log(2)/20;% kinf3 = p(5); % fusion
% p(6)  = log(2)/5;  %kunc4 = log(2)/5 min-1
% 
% p(7)  = 20 ; %len = p(6) ;      %20 um  % MT length
% 
% p(8)  = 1 ;  %v4  = p(7) ;       % random walk transient rate 1 um /s
% 
% p(9)  = .02;      %deg6 = 0.002; % degradation
% p(10)  = log(2)/30;% krt7 = p(9); % RT rate
% p(11)  = 1e-16;    % kb8 = p(8);% binding
% p(12)  = log(2)/30; %kd8 = p(10);    % desociation to the NPC
% 
% p(13)  = 0.002;%kim9 = p(11);    % nuclear import rate
% 
% p(14)  =  .075;%kint10 =  p(12);   % integration rate
% 
% p(15)  =  .075;%kcir11 =  p(13);   % circulate rate, LTR1
% 
% p(16)  = .075; %kcir12 = p(14);   % circulate rate, LTR2
% 
% p(17)  = 1e-12;%V = p(16);      % cell size
% % rw part
% 
% p(18) = 1; %a1 = 1; % -1
% p(19) = 1;%a2 = 1; % 0
% p(20) = 1.1;%a3 = 1.1; % +1
load par.mat
tic

% Total virions
nVirion = 200;

for i = 1:nVirion
    dflagtmp = 0 ; %deg flag
    [rwtmp ytmp dflagtmp]= runSingle(p);
    rw{i}=rwtmp;
    y{i}=ytmp;
    dflag(i) = dflagtmp;
end
toc

%% analyze the data
% rw part
for i = 1: nVirion
    MTarrive(i) = rw{i}(1,1)/60 ; %unit mins
    rw{i}(:,1)  = rw{i}(:,1) - rw{i}(1,1); %
    MTstay (i)  = rw{i}(end,1)/60;
    % find out the IDs who are degraded during the rw
    FateDtime (i) = y{i}(end,1);
    if y{i}(end,end) % 2LTR
        Fate (i) = 1;
    elseif y{i}(end,end-1) %1LTR
        Fate (i) = 2;
    elseif y{i}(end,end-2) %Integrated
        Fate(i) = 3;
    else 
        Fate(i) = 4;  % Degraded
    end
end


%% find out arrive virions
arriveId = find(Fate <4);
deathId  = find(Fate ==4); 
count = numel(arriveId);
countd = numel(deathId);
disp('Survival rate = ')
disp((count-1)/nVirion)

%% save
save r200.mat


%% MT stay time
load r200.mat

%% fig.2 fate decision
figure('position',[30 37 1035 903])
%
for i = 1 : nVirion
    % Fate
    subplot(2,2,3)
    plot(Fate(i)+(rand-0.5)*0.5,i,'.','color',colr(i,:),'markersize',20)
    hold on
    %     hist(Fate)
    axis tight
    set(gca,'xtick',1:4,'xticklabel',{'2LTR','1LTR','Integrated','Degraded'})
    ylabel('Virion id')
end

% label the bar 
[a,~] = hist(Fate,4);
gtext(num2str(a(1)),'color','r')
gtext(num2str(a(2)),'color','r')
gtext(num2str(a(3)),'color','r')
gtext(num2str(a(4)),'color','r')

%% fig.2b
subplot(2,2,1)
hist(FateDtime(deathId(:)),0:10:400)
xlabel('Total time (min)')
ylabel('Counts')
axis tight
 set(gca,'box','off','TickDir','out')

%%
% disp('Survival rate = ')
% disp(1-a(4)/200)
%% fig.2d
subplot(2,2,4)

h = pie(a)
set(h(1),'FaceColor','b') 
set(h(3),'FaceColor','g') 
set(h(5),'FaceColor','r') 
set(h(7),'FaceColor','y') 

names = {'Degraded','Integrated','1LTR','2LTR'};
for i =1 :4 
    gtext(names{i},'color','w')
end
title('Final fate','color','r')

%% fig.2b
subplot(2,2,2)
%hist(FateDtime,0:60:1920)
hist(FateDtime(arriveId(:)),0:120:1920)

ylabel('Counts')
axis tight
set(gca,'box','off','TickDir','out','xtick',0:240:1920,'xticklabel',0:4:32)
 
%%
% = findobj(gca,'Type','patch');
%set(h,'FaceColor','k','EdgeColor','w','facealpha',1)
%%
hold on
%%
% hist(FateDtime(arriveId(:)),0:60:3000)
% h = findobj(gca,'Type','patch');
% set(h,'FaceColor','y','EdgeColor','k','facealpha',0.75);



% %%
% open 200virions2.fig
% 
% %%
% saveas(gca,'modify.fig')
% saveEps('modify.eps')
% 
% %%
% close all