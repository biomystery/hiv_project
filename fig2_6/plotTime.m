% Fit the FateDtime by lognormal distribution 

function [m cv] = plotTime(FateDtime)
%%
%load rate5000
[n,x]= hist(FateDtime,0:30:max(FateDtime));
bar(x,n,'hist');

% dfdf
params = lognfit(FateDtime);
xx = linspace(0,max(FateDtime));
yy = lognpdf(xx,params(1),params(2)) * length(FateDtime) * (x(2)-x(1));
line(xx,yy,'color','r')

%% 
tmp = get(gca,'YTick');
set(gca,'yticklabel',tmp'/5000)
set(gca,'xlim',[0 60*120])%,'xtick',[])

%%
[m cv] =lognstat(params(1),params(2));
end