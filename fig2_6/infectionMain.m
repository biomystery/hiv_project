%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% infectionMain: perturbation selected parameters by modifier 
%% Time-stamp: <2013-08-04 Sun 06:12:54 UTC>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('~/Dropbox/matlab');
%% Original  parameters, unit min
load par.mat; 
nVirion = 5000;


modifier = [1/8 1/4 1/2 1 2 4 8]; 


%% 1. deg rate constant 
pnorm = p(9); 
prun = repmat(p,numel(modifier)); 
prun(:,9) = modifier'*pnorm;

 parfor i =1: numel(modifier) 
     [rateDeg(i) tmp]= infectionSingle(prun(i,:),nVirion); 
     rateFDtime{i} =tmp; 
 end 
% 
%% 2. traffic
p(9) = pnorm;

pnorm = p(8); 
for i =1: numel(modifier) 
    p(8)= modifier(i)* pnorm; 
    [rateTra(i) tmp]= infectionSingle(p,nVirion); 
    rateTraFDtime{i} =tmp; 
end


%% 3. fusion
p(8) = pnorm;

pnorm = p(5); 
for i =1: numel(modifier) 
    p(5)= modifier(i)* pnorm; 
    [rateFusion(i) tmp] = infectionSingle(p,nVirion); 
    rateFusionFDtime{i}=tmp; 
end

%% 4. RT
p(5) = pnorm;

pnorm = p(10); 
for i =1: numel(modifier) 
    p(10)= modifier(i)* pnorm; 
    [rateRT(i) tmp] = infectionSingle(p,nVirion); 
    rateRTFDtime{i} = tmp; 
end

%% 5. Integration
p(10) = pnorm;

pnorm = p(14); 
for i =1: numel(modifier) 
    p(14)= modifier(i)* pnorm; 
    [rateIntegration(i) tmp] = infectionSingle(p,nVirion); 
    rateInteFDtime{i} = tmp; 
end
p(14) = pnorm; 

save 20151231_0900am_rate50000.mat



