function ytmp = runSet1(y0,p)
% run reaction set one;
% update the time and the state;

%% parameters, unit min
% set 1
kb1   = p(1); % binding to CD4
kd1   = p(2); % unbinding from cd4
kb2   = p(3); % binds to the ccr5
kd2   = p(4); % unbind from the ccr5
kinf3 = p(5); % fusion
kunc4 = p(6); % uncoating
% other
V = p(17);      % cell size

ytmp(1,1)  = y0(1);  % vironfree
ytmp(1,2)  = y0(2);  % cd4pm
ytmp(1,3)  = y0(3);  % ccr5pm
ytmp(1,4)  = y0(4);  % viron:cd4
ytmp(1,5)  = y0(5);  % viron:cd4:ccr
ytmp(1,6)  = y0(6);  % virion(x)
ytmp(1,7)  = y0(7);  % RTC(x)
ytmp(1,8)  = y0(8);  % PIC
ytmp(1,9)  = y0(9);  % npcnuc
ytmp(1,10) = y0(10); % picdoc
ytmp(1,11) = y0(11); % PICnuc
ytmp(1,12) = y0(12); % vironint
ytmp(1,13) = y0(13); % 1LTRnuc
ytmp(1,14) = y0(14); % 2LTRnuc


% index and time 
i  = 1; 
t(i) = 0;

%% state update agorithm
% set 1

while ne(ytmp(i,7),1) % uncoatting? 
    
    p1f = kb1 * ytmp(i,1) * ytmp(i,2)/V;
    p1b = p1f + kd1 * ytmp(i,4);
    p2f = p1b + kb2 * ytmp(i,4) * ytmp(i,3)/V ;
    p2b = p2f + kd2 * ytmp(i,5);
    p3  = p2b + kinf3 * ytmp(i,5);
    p4  = p3  + kunc4 * ytmp(i,6);
    
    % update state
    s2 = rand * p4;
    
    z1 = (s2<=p1f);         %1f binds to cd4
    z2 = (s2<=p1b)&(s2>p1f);%1b unbind from cd4
    z3 = (s2<=p2f)&(s2>p1b);%2f binds to ccr5
    z4 = (s2<=p2b)&(s2>p2f);%2b unbind from ccr5
    z5 = (s2<=p3)&(s2>p2b); %3  fusion 
    z6 =  s2> p3;           %4  uncoating
    
    % update time
    i    = i + 1;
    t(i) = t(i-1)-log(rand)/p4;
    
    ytmp(i,1) =  max(0,ytmp(i-1,1)  - z1 + z2);           % vironfree
    ytmp(i,2) =  max(0,ytmp(i-1,2)  - z1 + z2 + z5);      % cd4
    ytmp(i,3) =  max(0,ytmp(i-1,3)  - z3 + z4 + z5);      % ccr5
    ytmp(i,4) =  max(0,ytmp(i-1,4)  + z1 - z2 - z3 + z4); % vrion:cd4
    ytmp(i,5) =  max(0,ytmp(i-1,5)  + z3 - z4 - z5);      % vrion:cd4:ccr5
    ytmp(i,6) =  max(0,ytmp(i-1,6)  + z5 - z6 );          % virionx=0
    ytmp(i,7) =  max(0,ytmp(i-1,7)  + z6 );               % RTCx=0
    ytmp(i,8) =  ytmp(i-1,8);
    ytmp(i,9) =  ytmp(i-1,9);
    ytmp(i,10)=  ytmp(i-1,10);
    ytmp(i,11)=  ytmp(i-1,11);
    ytmp(i,12)=  ytmp(i-1,12);
    ytmp(i,13)=  ytmp(i-1,13);
    ytmp(i,14)=  ytmp(i-1,14);
end

%% output
ytmp  = [t' ytmp]; % 1th column is the time
end

