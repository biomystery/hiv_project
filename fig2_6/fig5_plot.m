%% analysis  save rate5000.mat rate*
load 20151225_1020pm_rate5000.mat
% plot 
close all
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0 0.75 0.75;
      0.75 0 0.75;
      0.75 0.75 0;
      0.25 0.25 0.25];
set(groot,'defaultAxesColorOrder',co)
addpath('~/Dropbox/matlab');


figure('position',[690   351   460   354])
%plot(modifier,[rateDeg;rateTra;rateFusion;rateRT;rateIntegration],['-' ...
%                    'o'],'linewidth',2);
plot_data =[rateDeg;rateTra;rateFusion;rateRT;rateIntegration];
% plot_data(:,4) = ones(1,5)*mean(plot_data(:,4));
plot(modifier,plot_data,['-' ...
                    'o'],'linewidth',2,'MarkerSize',10);

legend('Degradation rate','Trafficking speed','Fusion rate',['RT ' ...
                    'rate'],'Integration rate','location','best')

set(gca,'xscale','log','fontsize',16) 
xlim([1/8 8.01])
set(gca,'xtick',modifier,'xticklabel',modifier); 

xlabel('Multiplier')
ylabel('Integration fraction') 
%
y=ylim;
for i = modifier
    h = line([i i],y);
    set(h,'color','k','linestyle','--')
end

%
figDir = '~/Dropbox/Current/3.Hiv_project/HIVpaper/2nd_submission/figures/';
%%
saveas(gca,[figDir,'fig5.fig'])
save2pdf([figDir,'fig5'])


