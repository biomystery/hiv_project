%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time-stamp: "2013-08-04 Sun 06:12:03 UTC"
% Run the model many times and get the statistics. Each time uses one virus
% infect one CD4+ T cell. 
% Require:
%     function [rw,y,dflag] =runSingle(p)
% Input: 
%     p - parameters, passed to runSingle 
%     nVirion - number of simulations 
% Output: 
%     inteRate - integration ratial 
%     fateDtime - fate decision time for each virion
% Comment:
%     possible using pararell computing toolbox.     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [inteRate fateDtime] = infectionSingle(p,nVirion)
    
for i = 1:nVirion
    dflagtmp = 0 ; %deg flag
    [rwtmp ytmp dflagtmp]= runSingle(p);
    rw{i}=rwtmp;
    y{i}=ytmp;
    dflag(i) = dflagtmp;
end

%% analyze the data
for i = 1: nVirion
    MTarrive(i)  = rw{i}(1,1)/60 ; %unit mins
    rw{i}(:,1)   = rw{i}(:,1) - rw{i}(1,1); %
    MTstay (i)   = rw{i}(end,1)/60;
    fateDtime(i) = y{i}(end,1);
    
    if y{i}(end,end) % 2LTR
        Fate (i) = 1;
    elseif y{i}(end,end-1) %1LTR
        Fate (i) = 2;
    elseif y{i}(end,end-2) %Integrated
        Fate(i)  = 3;
    else 
        Fate(i)  = 4;  % Degraded
    end
end

%% output
inteRate = numel(find(Fate==3))/nVirion;
fateDtime = fateDtime(find(Fate==3));
end