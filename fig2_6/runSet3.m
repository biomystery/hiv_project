function ytmp =runSet3(y0,p)
% run reaction set 3
%% parameters, unit min

kb8    = p(11);   % unbinds to the NPC
kub8   = p(12);   % unbinds to the NPC
kim9   = p(13);   % nuclear import rate
kint10 = p(14);   % integration rate
kcir11 = p(15);   % circulate rate, LTR1
kcir12 = p(16);   % circulate rate, LTR2
% other
V      = p(17);   % cell size

%% init
ytmp(1,1)  = y0(1);  % vironfree
ytmp(1,2)  = y0(2);  % cd4pm
ytmp(1,3)  = y0(3);  % ccr5pm
ytmp(1,4)  = y0(4);  % viron:cd4
ytmp(1,5)  = y0(5);  % viron:cd4:ccr
ytmp(1,6)  = y0(6);  % virion(x)
ytmp(1,7)  = y0(7);  % RTC(x)
ytmp(1,8)  = y0(8);  % PIC
ytmp(1,9)  = y0(9);  % npcnuc
ytmp(1,10) = y0(10); % picdoc
ytmp(1,11) = y0(11); % PICnuc
ytmp(1,12) = y0(12); % vironint
ytmp(1,13) = y0(13); % 1LTRnuc
ytmp(1,14) = y0(14); % 2LTRnuc
i = 1;
t(i) = 0 ; 
%% Run set three

while ne((ytmp(i,12) + ytmp(i,13) + ytmp(i,14)),1)% not reach the final & survivaled
  
    p8f = kb8 * ytmp(i,8) * ytmp(i,9)/V; % binding on NPC 
    p8b = p8f + kub8      * ytmp(i,10);
    p9  = p8b + kim9      * ytmp(i,10);
    p10 = p9  + kint10    * ytmp(i,11);
    p11 = p10 + kcir11    * ytmp(i,11);
    p12 = p11 + kcir12    * ytmp(i,11);
    
    % choose random #
    s2 = rand * p12;
    
    z6 = (s2<=p8f);         %8f 
    z7 = (s2<=p8b)&(s2>p8f);%8b
    z8 = (s2<=p9 )&(s2>p8b); %9
    z9 = (s2<=p10)&(s2>p9 );  %10
    z10= (s2<=p11)&(s2>p10);  %11
    z11= (s2> p11);           %12
    
    % time for next reaction
    i    = i + 1;
    t(i) = t(i-1)-log(rand)/p12;
    
    ytmp(i,1)  = ytmp(i-1,1);                   %vironfree
    ytmp(i,2)  = ytmp(i-1,2);                   % cd4
    ytmp(i,3)  = ytmp(i-1,3);                   % ccr5
    ytmp(i,4)  = ytmp(i-1,4);                   %vrion:cd4
    ytmp(i,5)  = ytmp(i-1,5);                   % vrion:cd4:ccr5
    ytmp(i,6)  = ytmp(i-1,6);                   % virion(0)
    ytmp(i,7)  = ytmp(i-1,7);                   % RTC(0)
    ytmp(i,8)  = max(0,ytmp(i-1,8)  - z6 + z7); %PIC
    ytmp(i,9)  = max(0,ytmp(i-1,9)  - z6 + z7); %NPCnuc
    ytmp(i,10) = max(0,ytmp(i-1,10) + z6 - z7 - z8  ); % PICdoc
    ytmp(i,11) = max(0,ytmp(i-1,11) + z8 - z9 - z10 - z11); %picnuc
    ytmp(i,12) = max(0,ytmp(i-1,12) + z9); % Virionint
    ytmp(i,13) = max(0,ytmp(i-1,13) + z10); % 1lTRnuc
    ytmp(i,14) = max(0,ytmp(i-1,14) + z11); % 2lTRnuc
   
end

%% output
ytmp  = [t' ytmp]; % 1th column is the time
end

