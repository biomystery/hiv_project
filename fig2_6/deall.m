% Draw FDT changes as the parameter changes. 

load FateDtime.mat
[n,x]= hist(FateDtime,0:60:max(FateDtime))

%
params = lognfit(FateDtime);
xx = linspace(0,max(FateDtime));
yy = lognpdf(xx,params(1),params(2)) * length(FateDtime) * (x(2)-x(1))/5000;
line(xx,yy,'color','r')

%%
load rate5000.mat

%%
for i = 1:7 
    subplot(7,1,i)
    [m(i) cv(i)]= plotTime(rateFDtime{i})
end

%% 
modifier = [1/8 1/4 1/2 1 2 4 8]; 
for i = 1:7 
    FDdegMean(i) = mean(rateFDtime{i});
    FDdegStd(i) = std(rateFDtime{i});
    FDdegCV(i) = FDdegStd(i) /FDdegMean(i);
    
    FDfuMean(i) = mean(rateFusionFDtime{i});
    FDfuStd(i) = std(rateFusionFDtime{i});
    FDfuCV(i) = FDfuStd(i) /FDfuMean(i);
    
    FDtrMean(i) = mean(rateTraFDtime{i});
    FDtrStd(i) = std(rateTraFDtime{i});
    FDtrCV(i) = FDtrStd(i) /FDtrMean(i);
end
%%
figure
subplot(2,1,1)
plot(modifier, [FDdegMean;FDtrMean;FDfuMean],'-o')
xlim([1/8 8.01])
set(gca,'xtick',modifier,'xticklabel',modifier,'xscale','log'); 


subplot(2,1,2)
plot(modifier, [FDdegStd;FDtrStd;FDfuStd],'-o')

xlim([1/8 8.01])
set(gca,'xtick',modifier,'xticklabel',modifier,'xscale','log'); 
legend('Degradation rate','Trafficking speed','Fusion rate')

