load r200.mat
figure('position',[30 37 1035 903])
%%
for i = 1 : nVirion
    % Fate
    subplot(2,2,3)
    plot(Fate(i)+(rand-0.5)*0.5,i,'.','color',colr(i,:),'markersize',20)
    hold on
    %     hist(Fate)
    axis tight
    set(gca,'xtick',1:4,'xticklabel',{'2LTR','1LTR','Integrated','Degraded'})
    ylabel('Virion id')
end

% label the bar
[a b] = hist(Fate,4);
gtext(num2str(a(1)),'color','r')
gtext(num2str(a(2)),'color','r')
gtext(num2str(a(3)),'color','r')
gtext(num2str(a(4)),'color','r')

%% fig.2b
subplot(2,2,1)
hist(FateDtime(deathId(:)),0:10:400)
xlabel('Total time (min)')
ylabel('Counts')
axis tight
 set(gca,'box','off','TickDir','out')

%%
% disp('Survival rate = ')
% disp(1-a(4)/200)
%% fig.2d
subplot(2,2,4)

h = pie(a)
set(h(1),'FaceColor','b')
set(h(3),'FaceColor','g')
set(h(5),'FaceColor','r')
set(h(7),'FaceColor','y')

names = {'Degraded','Integrated','1LTR','2LTR'};
for i =1 :4
    gtext(names{i},'color','w')
end
title('Final fate','color','r')

%% fig.2b
subplot(2,2,2)
%hist(FateDtime,0:60:1920)
hist(FateDtime(arriveId(:)),0:120:1920)

ylabel('Counts')
axis tight
set(gca,'box','off','TickDir','out','xtick',0:240:1920,'xticklabel',0:4:32)

%%
% = findobj(gca,'Type','patch');
%set(h,'FaceColor','k','EdgeColor','w','facealpha',1)
%%
hold on
%%
% hist(FateDtime(arriveId(:)),0:60:3000)
% h = findobj(gca,'Type','patch');
% set(h,'FaceColor','y','EdgeColor','k','facealpha',0.75);



% %%
% open 200virions2.fig
%
% %%
% saveas(gca,'modify.fig')
% saveEps('modify.eps')
%
% %%
% close all