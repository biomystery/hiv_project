% load 20151225_1050pm_rate5000.mat
load 20151225_1020pm_rate5000.mat
% load rate5000c.mat
% plot 
close all
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0 0.75 0.75;
      0.75 0 0.75;
      0.75 0.75 0;
      0.25 0.25 0.25];
set(groot,'defaultAxesColorOrder',co)
addpath('~/Dropbox/matlab');


figure('position',[688   222   467   483])
subplot 211
modifierMarker ={'0.125','0.25','0.5','1','2','4','8'};
plot(modifier,[calMeanFDT(rateFDtime);calMeanFDT(rateTraFDtime);calMeanFDT(rateFusionFDtime);calMeanFDT(rateRTFDtime);calMeanFDT(rateInteFDtime)],['-' ...
                    'o'],'linewidth',2,'markersize',10);
legend('Degradation rate','Trafficking speed','Fusion rate',['RT ' ...
                    'rate'],'Integration rate','location','best')

set(gca,'xscale','log','fontsize',16) 
set(gca,'xtick',modifier,'xticklabel',modifierMarker); 
subplot 212
plot(modifier,[calStdFDT(rateFDtime);calStdFDT(rateTraFDtime);calStdFDT(rateFusionFDtime);calStdFDT(rateRTFDtime);calStdFDT(rateInteFDtime)],['-' ...
                    'o'],'linewidth',2,'markersize',10);

set(gca,'xscale','log','fontsize',16) 
set(gca,'xtick',modifier,'xticklabel',modifierMarker); 
ylim([500 1000])
% saveas(gca,[figDir,'fig7bc.fig'])
% save2pdf([figDir,'fig7bc'])

%% 
figure
nplots = length(rateFDtime);
for i =1:nplots
    subplot(nplots,1,i)
    plotTime(rateFDtime{i})
    xlim([0 25*60])
    set(gca,'xtick',0:300:(25*60),'xticklabel',0:5:25)
end