
******************************************************
*            PART I (run multiple infections or samples) 
******************************************************

- mainSingle.m (generate the statistics of the model)

--- runSingle.m (run the model one time, [rw ytmp dflag]=runSingle(p)
                  get the random walk timecourse and y (states timecourse)
                    for each simulation)

----- runSet1.m

----- runSet2.m

----- runSet3.m

----- randomWalk.m

--- par.mat (provide parameters for the model) 

Output: 

- r_numberOfInfection.mat: 

    1. IDs for each infection 
    2. Fate for each ID
        2.1 integrated 
        2.2 Dead
        2.3 LTRs
    3. Time course for each ID (which stage at t) 

******************************************************
*                  PART II (random walk statistics) 
******************************************************
- runRW.m  (sample the RW statistics, reached or not (dead))

--- randomWalk.m

--- par.mat (provide parameters for the model) 


******************************************************
*          PART III (perturbation analysis)
******************************************************
- infectionMain.m (change one parameter a time to see how infection statistics
                    changes with that)

--- infectionSingle.m (run input_number times of infection, get FateDecisionTime
                        and other quantities) 

----- runSingle.m 
                    
--- par.mat


******************************************************
*          PART IV (analysis related to PART I)
******************************************************
- plotTime.m (FateDecisionTime statistics) 

--- need: FateDtime from mainSingle.m or infectionSingle.m

- avgTrajFinal.m

- plotBulkTimeCourse.m

--- avgTraj.m (get average states vs. time) 


******************************************************
*          PART V  Essential Models
******************************************************

- par.mat (parameters) 

- init.mat (y0) 

- runSet1.m

- runSet2.m

- runSet3.m

- randomWalk.m



