
function [t x] = randomWalk(p)
% return trajactory

%% parameters
% L  = 20 ;
% a1 = 1; % -1
% a2 = 1; % 0
% a3 = 3; % +1
% v  = 2.4; % 1um/s
L  = p(7) ;
a1 = p(18);% = 1; % -1
a2 = p(19);% = 1; % 0
a3 = p(20);% = 1.1; % +1

v  = p(8); % 1um/s
% init
t(1) = 0;
x(1) = 0;

%% update or Walk
i     = 1;

while (x(i) <= L) 
    %     disp (i);
    if x(i)<=0 % return to the beginning or init
        
        x(i) = 0; % get rid of minus location
        
        % generate rand1 & tau & prepare for update
        at = a2 + a3;
        RR = rand * at; % generate random number
        tau = -(1/at)*log(rand);
        i = i + 1;
        
        % choose direction & Update
        if RR < a2 % s = 0, stop
            s = 0;
        else  % step forward with speed v
            s = 1;
        end
        % update
        x(i) = x(i-1) + v * s * tau;
        t(i) = t(i-1) + tau     ;

    else % walk in the middle
        
        % generate rand1 & tau & prepare for update
        at = a1 + a2 + a3;
        RR = rand * at; % generate random number
        tau = -(1/at) * log(rand);
        i = i + 1;
        
        % choose direction & Update
        if RR < a1 % s = -1, step backwards
            s = -1;
        elseif  RR < (a1 + a2) % s=0 step forward with speed v
            s = 0 ;
        else %forward
            s = 1 ;
        end
        % update
        x(i) = x(i-1) + v * s * tau;
        t(i) = t(i-1) + tau     ;
    end
end

% reach the end
steps = i ;       %total steps
x(i)  = L;
speed = L / t(i); %average speed
end

