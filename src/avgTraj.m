function avg = avgTraj(y,ids,tmax)
ncell = length(y)/65;
avg=zeros(ceil(tmax)+1,1);
for i = 1:length(y)
    yn = transformTCdata(y{i}(:,1),y{i}(:,ids));
    avg = avg + [yn; zeros(ceil(tmax)-ceil(y{i}(end,1)),1)];% set the rest with 0;
   
end 
 avg = avg/ncell; % copy per cell
end