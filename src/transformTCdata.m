function yn = transformTCdata(t,y)
% input time and value 
% output new value vector with bin = 1, size is max(time)+1
j=0; 
yn = zeros(ceil(t(end))+1,1);
yn(1) = 0; % starts value = 0
for i =1:ceil(t(end))
    j=j+(t(j+1)>=(i-1)&&t(j+1)<i); % update; starts from time 0
    %disp(j)
    yn(i+1) = y(j);
end
end 

