load r10k.mat
yname ={'vironfree','cd4pm','ccr5pm', 'viron:cd4', 'viron:cd4:ccr', ...
        'virionx','RTCx','PIC', 'npcnuc', 'picdoc','PICnuc', ...
        'vironint','1LTRnuc', '2LTRnuc'};

%%
nbin = 65; % 7 virus/cell
y = y(1:nbin);


%% late RT
maxTime = max(FateDtime(1:nbin)); 
picAvg = avgTraj(y,9,maxTime)+avgTraj(y,12,maxTime) + avgTraj(y,13,maxTime) ...
         + avgTraj(y,14,maxTime) + avgTraj(y,15,maxTime); 
% PIC + PICnuc 
subplot(3,1,1)
hold on 
plot((1:length(picAvg))/60,picAvg,'r')
title ('Late RT')
xlim([0,72])
set(gca,'xtick',0:12:72)

%% Aim 2: 2 LTR
tltrAvg = avgTraj(y,15,maxTime);
subplot(3,1,2)
hold on 
plot((1:length(tltrAvg))/60,tltrAvg,'r')
title ('2LTR')
xlim([0,72])
set(gca,'xtick',0:12:72)

%% Aim 3: provirion , id= 8  + 1 (time) 
provAvg = avgTraj(y,13,maxTime);
subplot(3,1,3)
hold on 
plot((1:length(provAvg))/60,provAvg,'r')
title('Provirion')
xlim([0,72])
set(gca,'xtick',0:12:72)



