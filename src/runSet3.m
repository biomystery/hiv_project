function ytmp =runSet3(y0,p)
% run reaction set 3
%% parameters, unit min

kim9   = p(11);   % nuclear import rate
kint10 = p(12);   % integration rate
kcir11 = p(13);   % circulate rate, LTR1
kcir12 = p(14);   % circulate rate, LTR2
% other
V      = p(17);   % cell size

% 
kdeg13  = p(21);   % degradation rate for provirion
kdeg14  = p(15);   % degradation rate for 1LTR
kdeg15  = p(16);   % degradation rate for 2LTR

%% init
ytmp(1,1)  = y0(1);  % vironfree
ytmp(1,2)  = y0(2);  % cd4pm
ytmp(1,3)  = y0(3);  % ccr5pm
ytmp(1,4)  = y0(4);  % viron:cd4
ytmp(1,5)  = y0(5);  % viron:cd4:ccr
ytmp(1,6)  = y0(6);  % virion(x)
ytmp(1,7)  = y0(7);  % RTC(x)
ytmp(1,8)  = y0(8);  % PIC
ytmp(1,9) = y0(9); % PICnuc
ytmp(1,10) = y0(10); % vironint
ytmp(1,11) = y0(11); % 1LTRnuc
ytmp(1,12) = y0(12); % 2LTRnuc
endFlag  = 0 ;    %ending flag

i = 1;
t(i) = 0 ; 
%% Run set three

while (endFlag ==0) & (t(i)< 72*60)% t<72 hour
  
    p9  = kim9      * ytmp(i,8);
    p10 = p9  + kint10    * ytmp(i,9);
    p11 = p10 + kcir11    * ytmp(i,9);
    p12 = p11 + kcir12    * ytmp(i,9);  
    p13 = p12 + kdeg13    * ytmp(i,10);  % provirus 
    p14 = p13 + kdeg14    * ytmp(i,11);  % 1ltr 
    p15 = p14 + kdeg15    * ytmp(i,12);  % 2ltr 

    % choose random #
    s2 = rand * p15;
    
    z8 = (s2<=p9 ); %9 , import
    z9 = (s2<=p10)&(s2>p9 );  %10, integration
    z10= (s2<=p11)&(s2>p10);  %11, 1 ltr cir
    z11= (s2<=p12)&(s2>p11);  %12, 2 ltr
    z12= (s2<=p13)&(s2>p12);  %13, provirion deg
    z13= (s2<=p14)&(s2>p13);  %14, 1ltr deg
    z14= (s2> p14);           %15, 2 ltr deg
    
    % time for next reaction
    i    = i + 1;
    t(i) = t(i-1)-log(rand)/p15;
    
    ytmp(i,1)  = ytmp(i-1,1);                   %vironfree
    ytmp(i,2)  = ytmp(i-1,2);                   % cd4
    ytmp(i,3)  = ytmp(i-1,3);                   % ccr5
    ytmp(i,4)  = ytmp(i-1,4);                   %vrion:cd4
    ytmp(i,5)  = ytmp(i-1,5);                   % vrion:cd4:ccr5
    ytmp(i,6)  = ytmp(i-1,6);                   % virion(0)
    ytmp(i,7)  = ytmp(i-1,7);                   % RTC(0)
    ytmp(i,8)  = max(0,ytmp(i-1,8)  - z8); %PIC
    ytmp(i,9) = max(0,ytmp(i-1,9) + z8 - z9 - z10 - z11); %picnuc
    ytmp(i,10) = max(0,ytmp(i-1,10) + z9  - z12); % Virionint
    ytmp(i,11) = max(0,ytmp(i-1,11) + z10 - z13); % 1lTRnuc
    ytmp(i,12) = max(0,ytmp(i-1,12) + z11 - z14); % 2lTRnuc
    endFlag = (z13|z12|z14);
end

%% output
ytmp  = [t' ytmp]; % 1th column is the time
end

