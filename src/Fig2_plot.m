load r200.mat
figure('position',[30 37 1035 903])

%
for i = 1:nVirion
    % count death rate and survival rate
    % rw time course
    subplot(3,2,[3 4])

    colr(i,:)=[rand rand rand];


    plot(rw{i}(:,1),rw{i}(:,2),'color',colr(i,:));
    title('Random walk trajectories of individual virion on MT','color','r')
    hold on ;

    xlabel('Time (s)')
    ylabel('x (\mum)')

    axis tight

    % mtarrive
    subplot(3,2,1)
    plot(MTarrive(i),i,'.','color',colr(i,:),'markersize',20)
    hold on
    xlabel('MT arrive time (min)')
    ylabel('virion id')

end
% identify the survival virus
subplot(3,2,[3 4])

hold on;
for i = 1:(count-1)
    plot(rw{arriveId(i)}(end,1),rw{arriveId(i)}(end,2),'s',...
        'MarkerEdgeColor','none',...
                'MarkerFaceColor',colr(arriveId(i),:),...
                'MarkerSize',10)%

end


%% MT stay time
h2=subplot(3,2,5)
cla(h2)
%
for i = 1:(countd-1)
    plot(MTstay(deathId(i)),deathId(i),'.',...
        'color',colr(deathId(i),:),'markersize',20)
    hold on
end
%
for i = 1:(count-1)
    plot(MTstay(arriveId(i)),arriveId(i),'s',...
        'MarkerEdgeColor','none',...
                'MarkerFaceColor',colr(arriveId(i),:),...
                'MarkerSize',5)%

end
 xlabel('MT stay time (min)')
 ylabel('Virion id')
%% hist of mt arrive
subplot(3,2,2)
hist(MTarrive(1:nVirion),16)
xlabel('MT arrive time (min)')
ylabel('Counts')
set(gca,'box','off','TickDir','out')
% hist of mt stay
subplot(3,2,6)
hist(MTstay(1:nVirion),16)
xlabel('MT stay time (min)')
ylabel('Counts')
set(gca,'box','off','TickDir','out')

