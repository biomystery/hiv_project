load('15-Dec-2015 16:15:56_parsScan_15p.mat')
%%
%rmsdFunc = @(x) sqrt(sum(x.^2))/numel(x); 
rmsResult = zeros(nSets,1); 
for i = 1:nSets
    rmsResult(i) = rms(pResult(:,i)); 
end

%%
tmp = objectfunc(parsFinal);
rmsNorm = rms(tmp(:)) ;

%%
[minRms,minInd] =min(rmsResult);
objectfuncCheck(parSpace(:,minInd));  

%%
hist(rmsResult)

%%
object
