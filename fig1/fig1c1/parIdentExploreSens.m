% explore the parameter indentifibility by sampling the parameter spaces 
% centered by the fitted parameter set 
%% load data 
clear; 
load expdata.mat ; load parsFinal_15p_a.mat; 

%% generate parameter sets 
nPturb = 64*25; 
pRange = [-1 1]; %log 10 scale
pturb = 2.^linspace(pRange(1),pRange(2),nPturb); 
nPar = 16;
% theta = [p(1:9);1.4/24;p(10:end)];
parsFinal=[parsFinal(1:9);1.4/24;p(10:end)];

parSpace =  cell(nPar,1); % 

% setup the parameter space 

for i = 1:nPar
    tmp = parsFinal * ones(1,nPturb); 
    tmp(i,:) = tmp(i,:).*pturb;
    parSpace{i} = tmp; 
end
    

%% run pars Scan 

% result build 
resiDim = numel(expdata(:,2:end)); 
rResult = cell(nPar,1); % nSets * residue dim 
rmsResult = cell(nPar,1); 

%
parpool(64)
tStart = tic;
disp(['start at:' datestr(now)]);

for pidx = 1: nPar
    rResultRun = zeros(resiDim,nPturb); 
    rmsResultRun = zeros(nPturb,1);
    parRunSpace = parSpace{pidx}; 
    parfor ptidx = 1:nPturb
        pars = parRunSpace(:,ptidx); 
        resid = objectfunc(pars); 
        rResultRun(:,ptidx) = resid(:); 
        rmsResultRun(ptidx) = rms(resid(:));
    end
    rResult{pidx} = rResultRun; 
    rmsResult{pidx} = rmsResultRun;
    disp(['Finish running ',num2str(pidx),' of ', num2str(nPar),'at', datestr(now)])
    
end

tSpent = toc; 


%%
tmp = clock;
save(['2015-12-30_',num2str(tmp(4)),num2str(tmp(5)),'_parsSens.mat']) 



