function ss = bulterss(theta,data)
% algae sum-of-squares function
% load expdata; 

time   = data.xdata;
ydata  = data.ydata;

% initial states

% initial states for the model
pReal = [theta(1:2) 0 theta(3:end)];
ymod = butlerfun(0:time(end),pReal);
ymodel = ymod(time+1,:);

% return 

ss = sum((ymodel - ydata).^2);