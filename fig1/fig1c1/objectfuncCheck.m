function ss= objectfuncCheck(p)
%
% return the residues between experimental data and the simulation
%

% Load the experimental data 
load expdata.mat; 
%load mcmcChain;


time  = expdata(:,1)-2;
ydata = expdata(:,2:end);

% initial states for the model
% y0 = zeros(6,1);
pReal = p;

ymod = butlerfun(0:time(end),pReal);
ymodel = ymod(time+1,:);



% return 
ss = sum(sum((ymodel - ydata).^2));

time = time +2; 
ymod = [zeros(2,4); ymod]; 

figure;
errorbar([time time time time],ydata,expError,'o','linewidth',1.5)%plot(time,ydata,'o','linewidth',1)
hold on 
plot(0:time(end),ymod,'linewidth',2); 

set(gca,'xtick',0:6:24)
xlim([0 24.5])
xlabel('Time (h)','fontsize',18)
ylabel('Level (normalized)','fontsize',18)
set(gca,'fontsize',16)
legend('Early RT','Late RT','2LTR','Provirion','location','best')


