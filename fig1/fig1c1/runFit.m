% Fit the 2LTR profile
%% inits:
% clear;clc;close all;

%% Experimental data. 
% Time 	Early_RT	Late_RT   	2LTR	Integrated
% expdata =[
% 2	2.105413156	0	0	0
% 4	9.328298392	0	0	0
% 6	22.43992804	3.987055343	0.30785018	0
% 8	52.94704405	33.16731137	2.385838892	0.81669791
% 10	84.29311911	70.21127769	10.18471011	3.811253065
% 12	125.0369605	106.3686699	33.3760903	20.14518817
% 14	126.4405693	103.6550114	52.41149307	63.15789859
% 16	112.4133877	93.41052933	46.66495639	77.04174766
% 18	88.22072995	89.09227282	47.97331965	94.73684595
% 20	94.67056162	99.3350694	75.67983581	121.0526354
% 22	82.82004239	95.90338702	84.17136993	88.20326652
% 24	100	100	100	100];
% 
% expError = [3.63301E-16	0	0	0
% 1.335882866	0	0	0
% 3.496554229	0	0	0
% 6.015199568	6.518794086	0	0.272236486
% 5.922842226	25.11934241	1.351417875	0.272236486
% 8.446937849	18.90553113	1.965686275	1.63339582
% 10.4749134	8.185375267	4.485008399	7.85238077
% 24.33881355	13.13864883	11.72266981	6.081210947
% 4.49202183	4.820842824	3.025453666	7.309825769
% 12.75523527	18.66165964	27.42895144	13.64514898
% 10.97416679	12.98002606	13.73826338	10.69356687
% 14.69988443	13.61442485	26.72078526	5.647316452];
% 
% 
% 
% data.xdata  = expdata(:,1);
% data.ydata  = expdata(:,2:end);
% 
% errRatio = expError./data.ydata;
% 
% % is nana
% expError(isnan(errRatio)) = 0.1;
% expError(find(errRatio<=0.02)) = 0.02*data.ydata(find(errRatio<=0.02));
% 
% save expdata.mat expdata; % save the expdata.


%% initial fit
load expdata.mat 

nPar = 15;
% First generate an initial chain.

parsFinal= ones(nPar,1)*.1;
% 
%   parsFinal(5:11) = .1;
% optionsLocal =optimset('TolFun',1e-10,'TolX',1e-10);

 for i =1:nPar
     lb =parsFinal/5;
     lb(9) = 1.4/24/1.5;
     lb(5) = lb(7)/10;
     lb(6) = lb(7)/10;
     ub =parsFinal*5;
     lb(end-2) = lb(end-1)/10;
     
     disp(i)
%     
     options =optimset('PlotFcns',{@optimplotx,@optimplotfunccount,@optimplotresnorm,@optimplotfirstorderopt},...
     'TolFun',1e-10,'TolX',1e-10); 
% 
     [parsFinal, chisq] = lsqnonlin(@objectfunc,parsFinal,lb,ub,options);
     disp(chisq)
     disp(parsFinal)
 end

%%

p = parsFinal;
theta = [p(1:9);1.4/24;p(10:end)];
save 12-13-15_0216pm_parsFinal_15p.mat 