function y=butlerfun(time,p)

theta = [p(1:9);1.4/24;p(10:end)];


sol = dde23(@butler,[theta(12)],@ddex1hist,time,[],theta);

y = sol.y;
y = deval(sol,time);
y = y';
y = [y(:,2) sum(y(:,3:end),2)  y(:,5) y(:,6)]; % eRT, lateRT 2lTR 'Provirion' 
                                             

% rescale. 
  y = [y(:,1)/theta(16) y(:,2)/theta(13) y(:,3)/theta(14) y(:,4)/theta(15)]; 
  %y = [y(:,1) y(:,2) y(:,3) y(:,4)]; th