function ss= objectfunc(p)
%
% return the residues between experimental data and the simulation
%

% Load the experimental data 
load expdata; 

time  = expdata(:,1)-2;
ydata = expdata(:,2:end);

% initial states for the model
pReal = p;
ymod = butlerfun(0:time(end),pReal);
ymodel = ymod(time+1,:);

% return 

ss = (ymodel - ydata);

%subplot 224
%plot(time,ydata,'o')
%hold on 
%plot(0:time(end),ymod)
%hold off