function ydot = butler(t,y,Z,theta)

ERTlag = Z(:,1);

V = y(1);
ERT = y(2);
D = y(3); % linear cDNA 
L1 = y(4);
L2 = y(5);
I  = y(6);


dotV   = theta(1)*theta(2)*exp(-theta(2)*t) - theta(11)*V; % V_in
dotERT = theta(11)*V -theta(3)*ERT;% -theta(17)*ERT;
dotD   = theta(3)*ERTlag(1) - (theta(4)+ theta(5)+theta(6)+theta(7))*D; 
dotL1  = theta(7)*D - theta(8)*L1;
dotL2   = theta(6)*D - theta(9)*L2; 
dotI   = theta(5)*D - theta(10)*I; 

ydot=[dotV;dotERT;dotD;dotL1;dotL2;dotI];