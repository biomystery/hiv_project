% explore the parameter indentifibility by sampling the parameter spaces 
% centered by the fitted parameter set 

%%
clear;close all; 
%%
load('2015-12-15_1952_parsSens.mat')
% % load 2015-15-15_18_parsScan4.mat
load('2015-12-15_1952_parsSensResult.mat','rmsPlot')
%%
% rmsData = cell2mat(rmsResult'); 
% 
% tmp = objectfunc(parsFinal);
% rmsNorm = rms(tmp(:)) ;
% 
% rmsPlot = ((rmsData-rmsNorm)/rmsNorm)*100; 
 nPar = nPar+1;
%%
 figure 
imagesc(1:nPar,log10(pturb),rmsPlot,[min(rmsPlot(:)) 100]); hc =colorbar;
yticks = [1./[10 5 4 2] 1 2 4 5 10];
set(gca,'xtick',1:nPar)%,'ytick',yticks)
xlabel('Parameter index'); ylabel('Perturb multiplier (log10)') ;

ylabel(hc,'Percent increase in RMSD'); 
hold on ; 
arrayfun(@(x) plot([x x], [-1 1], 'color',[.75 .75 .75]), 1.5:nPar)

%%
addpath('~/Dropbox/matlab')
save2pdf('parSens_1')

%% calculate 95% region
pCI = zeros(nPar,2);
parsFinal = [p(1:9);1.4/24;p(10:end)];

for i = 1:nPar
    ids = find(rmsPlot(:,i)<=5);
    pCI(i,:)= [pturb(ids(1)) pturb(ids(end))]*parsFinal(i);
end

%% 
lb = pCI(:,1); 
ub = pCI(:,2); 

save('2015-12-15_1952_parsSensResult.mat')

%%
figure
objectfuncCheckSens(parsFinal,1)
%
objectfuncCheckSens(lb,0)
objectfuncCheckSens(ub,0)
%%
figDir = '~/Dropbox/Current/3.Hiv_project/HIVpaper/2nd_submission/figures/';
saveas(gca,[figDir,'figS_par.fig'])
save2pdf([figDir,'figS_par'])