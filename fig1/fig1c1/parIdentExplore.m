% explore the parameter indentifibility by sampling the parameter spaces 
% centered by the fitted parameter set 
%% load data 
clear; 
load expdata.mat ; load parsFinal_15p_a.mat; 

%% generate parameter sets 
seed = rng; 
nSets =10^6; 
nPar = 15;
pRange = [-2 2]; %log 10 scale
parSpace = 10.^(rand(nPar,nSets)-.5)*range(pRange);  % 15 * 100 (sets) 
parSpace = parsFinal*ones(1,nSets).*parSpace; 

%% run pars Scan 

% result build 
resiDim = numel(expdata(:,2:end)); 
pResult = zeros(resiDim,nSets); % nSets * residue dim 
rmsResult = zeros(nSets,1); 

%
parpool(64)
tStart = tic;
disp(['start at:' datestr(now)]);

parfor pidx = 1: nSets
    resid = objectfunc(parSpace(:,pidx)); 
    pResult(:,pidx) = resid(:); 
    rmsResult(pidx) = rms(resid(:))
    if(~mod(pidx,10000))
        disp(['Finish running ',num2str(pidx),' of ', num2str(nSets),'at', datestr(now)])
    end
end
tSpent = toc; 


%%
tmp = clock;
save(['2015-15-',num2str(tmp(3)),'_',num2str(tmp(4)),'_parsScan',num2str(range(pRange)),'.mat']) 