%function plotBulkTimeCourse(dataFile)
%clc;clear
%load ./results/r5000.mat
%load dataFile
load r10000.mat
% Aim 1: PIC , id= 8  + 1 (time) 
% check the data sturcture 
%whos y % it is a cell with a length of 5000 

%% late RT
% largest end time 
maxTime = 24*60; %max(FateDtime); 
picAvg = avgTraj(y,9,maxTime)+avgTraj(y,12,maxTime)+avgTraj(y,13,maxTime)+avgTraj(y,14,maxTime)+avgTraj(y,15,maxTime); % PIC + PICnuc 
subplot(3,1,1)
hold on 
plot((1:length(picAvg))/60,picAvg,'r')
title ('Late RT')
xlim([0,maxTime/60])
set(gca,'xtick',0:12:24)
%set(gca,'yticklabel',(0:50:100)/1000)

%% Aim 2: 2 LTR
tltrAvg = avgTraj(y,15,maxTime);
subplot(3,1,2)
hold on 
plot((1:length(tltrAvg))/60,tltrAvg,'r')
title ('2LTR')
xlim([0,maxTime/60])
set(gca,'xtick',0:12:24)

%% Aim 3: provirion , id= 8  + 1 (time) 
provAvg = avgTraj(y,13,maxTime);
subplot(3,1,3)
hold on 
plot((1:length(provAvg))/60,provAvg,'r')
title('Provirion')
xlim([0,maxTime/60])
set(gca,'xtick',0:12:24)

%%


avg.tltrAvg = tltrAvg;
avg.proAvg = proAvg;
avg.picAvg = picAvg;

save avg.mat avg