function avg = avgTraj(y,ids,tmax)
% calculate the avrage profile from 0 to tmax
ncell = length(y);
avg=zeros(tmax+1,1);
for i = 1:length(y)
    yn = transformTCdata(y{i}(:,1),y{i}(:,ids),tmax);
    if length(yn) < length(avg)
        avg = avg + [yn;zeros(length(avg)-length(yn),1)];
    else 
        avg = avg + yn;% vending the rest with 0;
    end
    
   
end 
 avg = avg/ncell; % copy per cell
end