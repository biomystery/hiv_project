Plot/codes are from ver0.6_DNAdeg folder: sto_avg.fig
===
1. Run mainSingleCopy to get the r10000.dat
   — par.mat 
   — runSingle
      - runSet1
      - runSet2
      - randomWalk
      - runSet3
2. Run plotBulkTimeCourse to get avg. dat
   - avgTraj

3. Run plot_stoch_avg to get the sto_avg.fig.

