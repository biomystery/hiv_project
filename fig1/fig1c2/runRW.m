clc; clear;
%
% parameters is located in each subroutine, unit min
%% parameters, unit min
p(1)  = 6.72e-18; %kb1 = p(1); % binding to CD4
p(2)  = 0.09;    %kd1 = p(2);     % unbinding from cd4

p(3)  = 9e-18;   %kb2 = p(3);    % binds to the ccr5

p(4)  = log(2)/32;%kd2 = p(4);% unbind from the ccr5

p(5)  = log(2)/20;% kinf3 = p(5); % fusion

p(6)  = 20 ; %len = p(6) ;      %20 um  % MT length

p(7)  = 1 ;  %v4  = p(7) ;       % random walk transient rate 1 um /s

p(8)  = .002;%deg4 = 0.002; % degradation 
p(9)  = log(2)/30;% krt5 = p(9); % RT rate
p(10)  = 1e-16;    % kb6 = p(8);% binding
p(11)  = log(2)/30;%kd6 = p(10);    % desociation to the NPC

p(12)  = 0.002;%kim7 = p(11);    % nuclear import rate

p(13)  =  .075;%kint8 =  p(12);   % integration rate

p(14)  =  .075;%kcir9 =  p(13);   % circulate rate, LTR1

p(15)  = .075;%kcir10 = p(14);   % circulate rate, LTR2

p(16)  = 1e-12;%V = p(16);      % cell size
% rw part

p(17) = 1; %a1 = 1; % -1
p(18) = 1;%a2 = 1; % 0
p(19) = 1.1;%a3 = 1.1; % +1

%%
n = 200 ; 

tic
for i =1:n
    
    [t x] = randomWalk(p);
    L  = p(6) ;

    trac.t{i} = t;
    trac.x{i} = x; 
    speed(i)  = L / trac.t{i}(end);
    ttotal(i) = trac.t{i}(end) ; 

    disp (i) 
end
toc 
mean(speed) 

%%

figure('position',[10 40 900 600]) 
subplot(2,3,[1 2 4 5]) 
for i = 1:n
    plot(trac.t{i},trac.x{i});
    
    hold on ;
    
    xlabel('Time (s)')
    ylabel('x (\mum)')
end
title('200 trajactories','color','r') 
subplot(2,3,3) 
hist(ttotal) 
axis tight
xlabel('Total time (s)')
ylabel('Count')
title('Total time histogram','color','r') 

mean(ttotal); %21.375
std(ttotal); %5.5093

% text(30,40,'\mu = 21.4')
% text(30,35,'\sigma = 5.5')

subplot(2,3,6) 

% speed 
hist(speed) 
axis tight
title('Average speed histogram','color','r') 

xlabel('Speed (\mum/s)')
ylabel('Count')

mean(speed); %1.00
std(speed); %0.25
% text(1.2,35,'\mu = 1.00')
% text(1.2,30,'\sigma = 0.25')
% 
% % save
% 
% saveas(gca,'100Traj.fig')
% saveEps('100Traj.eps') 
% 


% close 

%% percentage of dead
count = 1 ;
countd = 1;
for i =1:n
    if trac.x{i}(end) ==20
        index(count) = i;
        ttotals (count) = trac.t{i}(end);
        count = count + 1;
    else
        index(countd) = i ;
        ttotald (countd) = trac.t{i}(end);
        countd = countd + 1;
    end
end

disp('Survival rate = ')
disp(count/n)

%% 
[n,xout] = hist(ttotals,101);

%%
parmhat = lognfit(ttotals/max(ttotals));%parmhat(1) = mu and parmhat(2) = sigma 

%%
bar(xout,n/numel(ttotals))
hold on ;
%
y = lognpdf(xout,parmhat(1),parmhat(2));

plot(xout,y,'r'); grid;
xlabel('x'); ylabel('p')
%

