function ytmp =runSet3(y0,p)
% run reaction set 3
%% parameters, unit min

kb8    = p(11);   % unbinds to the NPC
kub8   = p(12);   % unbinds to the NPC
kim9   = p(13);   % nuclear import rate
kint10 = p(14);   % integration rate
kcir11 = p(15);   % circulate rate, LTR1
kcir12 = p(16);   % circulate rate, LTR2
% other
V      = p(17);   % cell size

% 
kdeg13  = p(21);   % degradation rate for provirion
kdeg14  = p(22);   % degradation rate for 2LTR
kdeg15 = 0.129668333;%0.208633333;%.012; % degradation rate for 1LTR

%% init
ytmp(1,1)  = y0(1);  % vironfree
ytmp(1,2)  = y0(2);  % cd4pm
ytmp(1,3)  = y0(3);  % ccr5pm
ytmp(1,4)  = y0(4);  % viron:cd4
ytmp(1,5)  = y0(5);  % viron:cd4:ccr
ytmp(1,6)  = y0(6);  % virion(x)
ytmp(1,7)  = y0(7);  % RTC(x)
ytmp(1,8)  = y0(8);  % PIC
ytmp(1,9)  = y0(9);  % npcnuc
ytmp(1,10) = y0(10); % picdoc
ytmp(1,11) = y0(11); % PICnuc
ytmp(1,12) = y0(12); % vironint
ytmp(1,13) = y0(13); % 1LTRnuc
ytmp(1,14) = y0(14); % 2LTRnuc
endFlag  = 0 ;    %ending flag

i = 1;
t(i) = 0 ; 
%% Run set three

while (endFlag ==0) & (t(i)< 24*60)% t<72 hour
  
    p8f = kb8 * ytmp(i,8) * ytmp(i,9)/V; % binding on NPC 
    p8b = p8f + kub8      * ytmp(i,10);
    p9  = p8b + kim9      * ytmp(i,10);
    p10 = p9  + kint10    * ytmp(i,11);
    p11 = p10 + kcir11    * ytmp(i,11);
    p12 = p11 + kcir12    * ytmp(i,11);  %
    p13 = p12 + kdeg13    * ytmp(i,12);  % deg of provirion
    p14 = p13 + kdeg15    * ytmp(i,13);  % deg of 1ltr
    p15 = p14 + kdeg14    * ytmp(i,14);  % deg of 2ltr 

    % choose random #
    s2 = rand * p15;
    
    z6 = (s2<=p8f);         %8f 
    z7 = (s2<=p8b)&(s2>p8f);%8b
    z8 = (s2<=p9 )&(s2>p8b); %9
    z9 = (s2<=p10)&(s2>p9 );  %10
    z10= (s2<=p11)&(s2>p10);  %11
    z11= (s2<=p12)&(s2>p11);  %12
    z12= (s2<=p13)&(s2>p12);  %13
    z13= (s2<=p14)&(s2>p13);  %14
    z14= (s2> p14);           %15
    
    % time for next reaction
    i    = i + 1;
    t(i) = t(i-1)-log(rand)/p15;
    
    ytmp(i,1)  = ytmp(i-1,1);                   %vironfree
    ytmp(i,2)  = ytmp(i-1,2);                   % cd4
    ytmp(i,3)  = ytmp(i-1,3);                   % ccr5
    ytmp(i,4)  = ytmp(i-1,4);                   %vrion:cd4
    ytmp(i,5)  = ytmp(i-1,5);                   % vrion:cd4:ccr5
    ytmp(i,6)  = ytmp(i-1,6);                   % virion(0)
    ytmp(i,7)  = ytmp(i-1,7);                   % RTC(0)
    ytmp(i,8)  = max(0,ytmp(i-1,8)  - z6 + z7); %PIC
    ytmp(i,9)  = max(0,ytmp(i-1,9)  - z6 + z7); %NPCnuc
    ytmp(i,10) = max(0,ytmp(i-1,10) + z6 - z7 - z8  ); % PICdoc
    ytmp(i,11) = max(0,ytmp(i-1,11) + z8 - z9 - z10 - z11); %picnuc
    ytmp(i,12) = max(0,ytmp(i-1,12) + z9  - z12); % Virionint
    ytmp(i,13) = max(0,ytmp(i-1,13) + z10 - z14); % 1lTRnuc
    ytmp(i,14) = max(0,ytmp(i-1,14) + z11 - z13); % 2lTRnuc
    endFlag = (z13|z12|z14);
end

%% output
ytmp  = [t' ytmp]; % 1th column is the time
end

