function [rw ytmp dflag]=runSingle(p)
% input:   paramters a
% output:

%% parameters is located in each subroutine, unit min
% % parameters, unit min
% p(1)  = 6.72e-18; %kb1 = p(1); % binding to CD4
% p(2)  = 0.09;    %kd1 = p(2);     % unbinding from cd4
%
% p(3)  = 9e-18;   %kb2 = p(3);    % binds to the ccr5
%
% p(4)  = log(2)/32;%kd2 = p(4);% unbind from the ccr5
%
% p(5)  = log(2)/20;% kinf3 = p(5); % fusion
% p(6)  = log(2)/5;  %kunc4 = log(2)/5 min-1

% p(7)  = 20 ; %len = p(6) ;      %20 um  % MT length
%
% p(8)  = 1 ;  %v4  = p(7) ;       % random walk transient rate 1 um /s
%
% p(9)  = .002;      %deg6 = 0.002; % degradation
% p(10)  = log(2)/30;% krt7 = p(9); % RT rate
% p(11)  = 1e-16;    % kb8 = p(8);% binding
% p(12)  = log(2)/30; %kd8 = p(10);    % desociation to the NPC
%
% p(13)  = 0.002;%kim9 = p(11);    % nuclear import rate
%
% p(14)  =  .075;%kint10 =  p(12);   % integration rate
%
% p(15)  =  .075;%kcir11 =  p(13);   % circulate rate, LTR1
%
% p(16)  = .075; %kcir12 = p(14);   % circulate rate, LTR2
%
% p(17)  = 1e-12;%V = p(16);      % cell size
% % rw part
%
% p(18) = 1; %a1 = 1; % -1
% p(19) = 1;%a2 = 1; % 0
% p(20) = 1.1;%a3 = 1.1; % +1
% p(21) = log(2) /120
% p(22) = log(2) /90

%% init
y0 =  [1 46000 10000 0 0  ... % 1-5,  vironfree,cd4pm, ccr5pm, viron:cd4, viron:cd4:ccr
    0 0 0 2000 0          ... % 6-10  virionx,RTCx,PIC, npcnuc, picdoc
    0 0 0 0               ... % 11-14 PICnuc,vironint,1LTRnuc,2LTRnuc
    ];

%% Run set one
ytmp1 = runSet1(y0,p); % run set one
t(1)  = ytmp1(end,1);        % running time for set 1
ytmp  = ytmp1;

%% Run set two
ytmp2 = runSet2(ytmp1(end,2:end),p);
t(2)  = ytmp2(end,1);  % running time for set 2
% modify starting time
ytmp2(:,1) = ytmp2(:,1) + t(1);

% random walk
[trwTmp xrwTmp] = randomWalk(p); % change mins to secs
trw = trwTmp(end);            % running time for random walk

if ytmp2(end,9) % 8th col, 1st column is time, remain PIC
    
    dflag = 0;
    
    % update output
    rw = [trwTmp+t(1);xrwTmp]'; % unit of time in RW is min
    ytmp = [ytmp;ytmp2(2:end,:)] ; % start from the second time points
    
    
    if t(2) < trw        % RT during RW
        %  adding additional time for RW
        ytmp = [ytmp;trw + t(1)  ytmp2(end,2:end)];
    end
    
else % Degraded
    
    dflag = 1;
    
    % Output reactions set 2
    ytmp = [ytmp;ytmp2(2:end,:)]; % don't repeat 
    
    if t(2) < trw        % degraded during RW
        
        % update reactions
        ytmp = [ytmp;ytmp2(2:end,:)];
     
        % deal with random walk modification
        index = find(trwTmp > t(2));
        % new value for the degradtion location
        % yi = interp1(x,Y,xi)
        xrwTmp(index(1))= interp1([trwTmp(index(1)-1) trwTmp(index(1))],...
            [xrwTmp(index(1)-1) xrwTmp(index(1))],t(2));
        trwTmp(index(1)) = t(2);
        % output only till degradation at t(2)*60 secs
        rw = [trwTmp(1:index(1))+t(1); xrwTmp(1:index(1))]'; % unit of time in RW is secs.
        
    else                  % degraded after RW
        
        % output  rw
        rw = [trwTmp+t(1);xrwTmp]'; % unit of time in RW is secs.
    end
    % aborted
    return;
end

%% Run set Three
ytmp3 = runSet3(ytmp2(end,2:end),p); % run set Three
t(3)  = ytmp3(end,1);        % running time for set 1

% modify starting time
ytmp3(:,1) = ytmp3(:,1) + ytmp(end,1);

% update
ytmp = [ytmp;ytmp3(2:end,:)] ; % start from the second time points

end

