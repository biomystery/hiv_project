
load avg.mat
clf
plot(0:1440,avg.picAvg/avg.picAvg(19*60+1),'color',[0    0.4980         0],'linewidth',1.2)
hold on
plot(0:1440,avg.tltrAvg/avg.tltrAvg(19*60+1),'r','linewidth',1.2)
plot(0:1440,avg.proAvg/avg.proAvg(19*60+1),'color',[0,.75,.75],'linewidth',1.2)

xlim([-5*60 19*60])
set(gca,'fontsize',16)
set(gca,'xtick',-5*60:(6*60):(19*60))
set(gca,'xticklabel',0:6:24)
legend('late RT','2LTR','proviron')
%
xlabel('Time (h)')
ylabel('Level (normalized)')

saveas(gca,'sto_avg.fig')
saveEps('sto_avg.eps')
